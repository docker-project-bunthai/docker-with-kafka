FROM adoptopenjdk/openjdk8:jdk8u202-b08-slim
MAINTAINER Deng Bunthai
COPY ./build/libs/*.jar app.jar
ENTRYPOINT ["sh", "-c", "java -jar app.jar"]